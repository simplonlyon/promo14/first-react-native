import { createNativeStackNavigator, NativeStackNavigationProp } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { HomeScreen } from './Screen/HomeScreen';
import { ImageScreen } from './Screen/ImageScreen';
import { CameraScreen } from './Screen/CameraScreen';
import { SensorScreen } from './Screen/SensorScreen';

const Stack = createNativeStackNavigator();

export interface NavProps {
  navigation:NativeStackNavigationProp<any>
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Image" component={ImageScreen} />
        <Stack.Screen name="Camera" component={CameraScreen} />
        <Stack.Screen name="Sensor" component={SensorScreen} />
      </Stack.Navigator>

    </NavigationContainer>
  );
}
