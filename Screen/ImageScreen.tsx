import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import React from 'react';
import pug from '../assets/pug.jpg';

export function ImageScreen() {
    
    return (
        <View>
            <Text>Image screen</Text>
            <Image source={pug} />
            <TextInput placeholder="Email" autoCapitalize="none" keyboardType="email-address" style={styles.input} ></TextInput>
            <TextInput placeholder="Tél" keyboardType="phone-pad" style={styles.input} ></TextInput>
            
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
  });