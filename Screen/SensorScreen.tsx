import { StyleSheet, Dimensions, View, Image, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from 'react';
import { Accelerometer, ThreeAxisMeasurement } from 'expo-sensors'
import * as ImagePicker from 'expo-image-picker';

export function SensorScreen() {
    const [data, setData] = useState<ThreeAxisMeasurement>({
        x: 0,
        y: 0,
        z: 0
    });
    const [ballImage, setBallImage] = useState('osef');


    const [ballPosition, setBallPosition] = useState({
        left: 150,
        top: 200
    });

    useEffect(() => {
        Accelerometer.setUpdateInterval(10);
        const listener = Accelerometer.addListener((event) => {
            setData(event)
            setBallPosition(position => {
                let newLeft = position.left - (event.x * 10);
                let newTop = position.top + (event.y * 10);
                if(newLeft >= Dimensions.get('window').width-100 || newLeft <= 0) {
                    newLeft = position.left;
                }
                if(newTop >= Dimensions.get('window').height-160 || newTop <= 0) {
                    newTop = position.top;
                }

                return {
                    left: newLeft,
                    top: newTop
                }
            });

        });

        return () => listener.remove()
    }, []);

    const pickImage = async () => {
        const {status} = await ImagePicker.getCameraPermissionsAsync();
        if(status !== 'granted') {
            alert('authorization denied')
            return;
        }
        const result = await ImagePicker.launchCameraAsync();
        if(!result.cancelled) {
            setBallImage(result.uri);
        }
    }

    return (
        <View>
            <TouchableOpacity onPress={pickImage} style={{ ...styles.ball, left: ballPosition.left, top: ballPosition.top }}>
                <Image  source={{uri:ballImage}} style={{flex: 1, borderRadius:50}}/>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    ball: {
        width: 100,
        height: 100,
        position: "absolute",
        backgroundColor: 'red',
        borderRadius: 50
    }
})