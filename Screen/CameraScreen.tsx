import { Button, Image, StyleSheet, Text, View } from "react-native"; 
import React, { useEffect, useRef, useState } from 'react'
import { Camera } from "expo-camera";



export function CameraScreen() {
    const [hasPermission, setHasPermission] = useState<boolean | null>(null);
    const [takingPicture, setTakingPicture] = useState(false);
    const [picture, setPicture] = useState<string | undefined>(undefined);
    const cameraRef = useRef<Camera>(null);

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    })

    const takePicture = () => {
        setTakingPicture(true);
    }

    const handlePicture = async () => {
        if(cameraRef.current) {
            // cameraRef.current.pausePreview()
            const data = await cameraRef.current.takePictureAsync({base64: true, skipProcessing: true});
            setPicture(data.base64)
        }
        setTakingPicture(false);

    }

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>Camera Permission refused</Text>
    }

    return (
        <View style={styles.container}>
            {!takingPicture?
                <>
                    <Text>Camera Screen</Text>
                    {picture && <Image style={{flex:1}} source={{uri:`data:image/jpeg;base64,${picture}`}} />}

                    <Button onPress={takePicture} title="Take a picture" />
                </>
            :
            <>
                <Camera ref={cameraRef} type={Camera.Constants.Type.back} style={styles.camera}>
                    <View style={styles.buttonContainer}>
                        <Button onPress={handlePicture} title="Go"  />
                    </View>
                </Camera>
            </>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    camera: {
      flex: 1,
    },
    buttonContainer: {
      position:"absolute",
      bottom: 10,
      left: '10%',
      width: '80%'
    },
    button: {
      flex: 0.1,
      alignSelf: 'flex-end',
      alignItems: 'center',
    },
    text: {
      fontSize: 18,
      color: 'white',
    },
  });