import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { NavProps } from '../App';


export function HomeScreen({ navigation }: NavProps) {

  return (
    <View style={styles.container}>

      <View style={styles.box}>
        <TouchableOpacity onPress={() => navigation.navigate('Sensor')} style={{ backgroundColor: 'orange', height: '100%', width: '50%' }}>
          <Text>Sensor Screen</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Camera')} style={{ backgroundColor: 'green', height: '33.33%', width: '50%' }}>
          <Text>Camera screen</Text>

        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Image')} style={{ backgroundColor: 'red', height: '33.33%', width: '50%' }}>
          <Text>Image screen</Text>
        </TouchableOpacity>
        <View style={{ backgroundColor: 'black', height: '33.33%', width: '50%' }}></View>
      </View>
      <View style={{ ...styles.box, flex: 1, backgroundColor: 'blue' }}></View>
      <View style={{ ...styles.box, flexDirection: 'row' }}>
        <View style={{ backgroundColor: 'violet', height: '100%', flex: 1 }}></View>
        <View style={{ backgroundColor: 'yellow', height: '100%', flex: 1 }}></View>
      </View>

      <StatusBar style="auto" hidden />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  box: {
    flex: 3,
    borderStyle: 'solid',
    borderWidth: 1,
    flexWrap: 'wrap'
  },
});
